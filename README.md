毕业设计前段

### 下载代码及编译（需要安装node v6.3.0+）

```
mkdir -p ~/demo
cd ~/demo
git clone https://git.oschina.net/gongtiexin/graduationThesis.git
cd /graduationThesis/demo-frontend
npm install
npm run site-dev
npm run dev
```

### 配置 Nginx

新建`demo.conf`文件到 `$NGINX_HOME/conf.d` 目录:

```
server {
    listen       80;
    server_name  demo.test www.demo.test;

    charset utf-8;

    location /demo {
        proxy_pass http://localhost:20000;
        proxy_redirect off;
    }

    location / {
        root   /home/hldev/graduationThesis/demo-frontend/dist;
            try_files $uri /index.html =404;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

}

```
*（注意，修改`/home/hldev`为实际用户或项目所在目录，修改`proxy_pass`为实际代理地址）*


修改 `/etc/hosts` 文件，添加:

```
127.0.0.1       demo.test www.demo.test
```

打开浏览器访问: [http://demo.test/](http://demo.test/)