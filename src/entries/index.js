import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import routes from "../routes/routes";
import moment from "moment";
import "moment/locale/zh-cn";
import store from '../models/store';
moment.locale('zh-cn');

ReactDOM.render(<Provider store={store}>{routes}</Provider>, document.getElementById('root'));



