/**
 * Created by hldev on 17-2-9.
 */
import axios from "axios";
import {requestJSON, actionPromise, makeActionCreator} from "../serviceCommon";
import {signTYPE} from "../types";

export function signin(data) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'POST', url: `/demo/user/login`, data: data},
            {type: signTYPE.SIGN_IN, message: `登录成功`},
            {type: signTYPE.SIGN_IN_ERROR, message: `登录失败`});
}

export function signup(data) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'POST', url: '/demo/user', data: data},
            {type: signTYPE.SIGN_UP, message: '注册成功'},
            {type: signTYPE.SIGN_UP_ERROR, message: '注册失败'});
}

export function logout() {
    return dispatch => dispatch({type: signTYPE.LOGOUT});
}