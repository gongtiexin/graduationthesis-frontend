/**
 * Created by hldev on 17-2-9.
 */
import {fromJS} from 'immutable'
import {signTYPE} from '../types';

const initState = {};

function signReducer(state = fromJS(initState), action) {
    switch (action.type) {
        case signTYPE.SIGN_IN:
            return state.set(signTYPE.SIGN_IN, fromJS(action.data));

        case signTYPE.SIGN_UP:
            return state.set(signTYPE.SIGN_UP, fromJS(action.data));

        case signTYPE.LOGOUT:
            return state.remove(signTYPE.SIGN_IN);
            
        default:
            return state
    }
}

export default signReducer;