/**
 * Created by hldev on 17-2-9.
 */
import axios from "axios";
import {requestJSON, actionPromise, makeActionCreator} from "../serviceCommon";
import {recordsTYPE} from "../types";

export function findById(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'GET', url: `/demo/records/${id}`},
            {type: recordsTYPE.RECORDS,},
            {type: recordsTYPE.RECORDS_ERROR, message: `获取消息失败，消息ID：${id}`});
}

export function clearRecords() {
    return dispatch => dispatch({type: recordsTYPE.CLEAR_RECORDS});
}

export function findList() {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'GET', url: '/demo/records/list'},
            {type: recordsTYPE.RECORDS_LIST},
            {type: recordsTYPE.RECORDS_LIST_ERROR, message: '获取消息列表失败'});
}

export function findPage(params) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'GET', url: '/demo/records/page', params: params},
            {type: recordsTYPE.RECORDS_PAGE},
            {type: recordsTYPE.RECORDS_PAGE_ERROR, message: '获取消息列表失败'});
}

export function createRecords(data) {
    return dispatch => {
        requestJSON(
            dispatch,
            {method: 'POST', url: '/demo/records', data: data},
            {type: recordsTYPE.CREATE_RECORDS, message: '发表成功'},
            {type: recordsTYPE.CREATE_RECORDS_ERROR, message: '发表失败'});
    }
}

export function setStatus(records) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'PUT', url: '/demo/records', data: records},
            {type: recordsTYPE.UPDATE_RECORDS},
            {type: recordsTYPE.UPDATE_RECORDS_ERROR, message: '更新状态失败'})
}

export function deleteRecords(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'delete', url: `/demo/records/${id}`},
            {type: recordsTYPE.DELETE_RECORDS, message: '删除消息成功'},
            {type: recordsTYPE.DELETE_RECORDS_ERROR, message: '删除消息失败'});
}