/**
 * Created by hldev on 17-2-9.
 */
import {fromJS} from 'immutable'
import {recordsTYPE} from '../types';

const initState = {};

function recordsReducer(state = fromJS(initState), action) {
    switch (action.type) {
        case recordsTYPE.RECORDS:
            return state.set(recordsTYPE.RECORDS, fromJS(action.data));

        case recordsTYPE.CLEAR_RECORDS:
            return state.remove(recordsTYPE.RECORDS).remove(recordsTYPE.RECORDS_LIST).remove(recordsTYPE.RECORDS_ANOTHER).remove(recordsTYPE.RECORDS_PAGE);

        case recordsTYPE.RECORDS_LIST:
            return state.set(recordsTYPE.RECORDS_LIST, fromJS(action.data));

        case recordsTYPE.RECORDS_PAGE:
            return state.set(recordsTYPE.RECORDS_PAGE, fromJS(action.data));

        case recordsTYPE.CREATE_RECORDS:
            return state.set(recordsTYPE.CREATE_RECORDS, fromJS(action.data));

        case recordsTYPE.UPDATE_RECORDS:
            return state.set(recordsTYPE.UPDATE_RECORDS, fromJS(action.data));

        case recordsTYPE.RECORDS_ANOTHER:
            return state.set(recordsTYPE.RECORDS_ANOTHER, fromJS(action.data));

        default:
            return state
    }
}

export default recordsReducer;