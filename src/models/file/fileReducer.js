/**
 * Created by hldev on 17-2-15.
 */
import {fromJS} from 'immutable';
import {fileTYPE} from '../types';

const initState = {};

export default function fileReducer(state = fromJS(initState), action) {
    switch (action.type) {
        case fileTYPE.FILE:
            return state.set(fileTYPE.FILE, fromJS(action.data));

        case fileTYPE.CLEAR_FILE:
            return state.remove(fileTYPE.FILE).remove(fileTYPE.FILE_PAGE);

        case fileTYPE.FILE_PAGE:
            return state.set(fileTYPE.FILE_PAGE, fromJS(action.data));

        case fileTYPE.FILE_LIST:
            return state.set(fileTYPE.FILE_LIST, fromJS(action.data));

        case fileTYPE.CREATE_FILE:
            return state.set(fileTYPE.CREATE_FILE, fromJS(action.data));

        default:
            return state
    }
}