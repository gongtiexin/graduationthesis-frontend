/**
 * Created by hldev on 17-2-15.
 */
import {requestJSON} from "../serviceCommon";
import {fileTYPE} from '../types';
import Utils from '../../common/Utils';

export function findPage(params) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: '/demo/file/page', params: params},
            {type: fileTYPE.FILE_PAGE},
            {type: fileTYPE.FILE_PAGE_ERROR, message: '获取文件列表失败'});
}

export function findById(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: `/demo/file/${id}`},
            {type: fileTYPE.FILE},
            {type: fileTYPE.FILE_ERROR, message: '获取文件详情失败'});
}

export function update(file) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'PUT', url: '/demo/file', data: file},
            {type: fileTYPE.UPDATE_FILE, message: '编辑文件成功'},
            {type: fileTYPE.UPDATE_FILE_ERROR, message: '编辑文件失败'})
}

export function findList(params) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: '/demo/file/list', params: params},
            {type: fileTYPE.FILE_LIST},
            {type: fileTYPE.FILE_LIST_ERROR, message: '获取文件列表失败'});
}

export function clearFile() {
    return dispatch =>
        dispatch({type: fileTYPE.CLEAR_FILE});
}

export function deleteFile(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'delete', url: `/demo/file/${id}`},
            {type: fileTYPE.DELETE_FILE,message: '删除文件成功'},
            {type: fileTYPE.DELETE_FILE_ERROR, message: '删除文件失败'});
}