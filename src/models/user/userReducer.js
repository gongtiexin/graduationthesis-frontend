/**
 * Created by hldev on 17-2-4.
 */
import {fromJS} from 'immutable'
import {userTYPE} from '../types';

const initState = {};

function userReducer(state = fromJS(initState), action) {
    switch (action.type) {
        case userTYPE.USER:
            return state.set(userTYPE.USER, fromJS(action.data));

        case userTYPE.CLEAR_USER:
            return state.remove(userTYPE.USER).remove(userTYPE.USER_LIST);

        case userTYPE.USER_LIST:
            return state.set(userTYPE.USER_LIST, fromJS(action.data));

        case userTYPE.UPDATE_USER:
            return state.set(userTYPE.UPDATE_USER, fromJS(action.data));

        default:
            return state
    }
}

export default userReducer;