/**
 * Created by hldev on 17-2-4.
 */
import axios from "axios";
import {requestJSON, actionPromise, makeActionCreator} from "../serviceCommon";
import {userTYPE} from "../types";

export function findById(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'GET', url: `/demo/user/${id}`},
            {type: userTYPE.USER,},
            {type: userTYPE.USER_ERROR, message: `获取用户失败，用户ID：${id}`});
}

export function clearUser() {
    return dispatch => dispatch({type: userTYPE.CLEAR_USER});
}

export function findList() {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'GET', url: '/demo/user/list'},
            {type: userTYPE.USER_LIST},
            {type: userTYPE.USER_LIST_ERROR, message: '获取用户列表失败'});
}

export function deleteFile(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'delete', url: `/demo/user/${id}`},
            {type: userTYPE.DELETE_USER},
            {type: userTYPE.DELETE_USER_ERROR, message: '删除 用户失败'});
}