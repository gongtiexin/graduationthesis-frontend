/**
 * Created by Administrator on 2017/1/4.
 */
import {requestJSON} from "../serviceCommon";
import {articleTYPE} from '../types';
import Utils from '../../common/Utils';

export function findPage(params) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: '/demo/article/page', params: {...params, status: 1}},
            {type: articleTYPE.ARTICLE_PAGE},
            {type: articleTYPE.ARTICLE_PAGE_ERROR, message: '获取文章列表失败'});
}

export function findById(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: `/demo/article/${id}`},
            {type: articleTYPE.ARTICLE},
            {type: articleTYPE.ARTICLE_ERROR, message: '获取文章详情失败'});
}

export function create(article) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'post', url: '/demo/article', data: article},
            {type: articleTYPE.CREATE_ARTICLE, message: '发表文章成功'},
            {type: articleTYPE.CREATE_ARTICLE_ERROR, message: '发表文章失败'});
}

export function update(article) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'PUT', url: '/demo/article', data: article},
            {type: articleTYPE.UPDATE_ARTICLE, message: '编辑文章成功'},
            {type: articleTYPE.UPDATE_ARTICLE_ERROR, message: '编辑文章失败'})
}

export function findList(params) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'get', url: '/demo/article/list', params: params},
            {type: articleTYPE.ARTICLE_LIST},
            {type: articleTYPE.ARTICLE_LIST_ERROR, message: '获取文章列表失败'});
}

export function clearArticle() {
    return dispatch =>
        dispatch({type: articleTYPE.CLEAR_ARTICLE});
}

export function deleteArticle(id) {
    return dispatch =>
        requestJSON(
            dispatch,
            {method: 'delete', url: `/demo/article/${id}`},
            {type: articleTYPE.DELETE_ARTICLE, message: '删除文章成功'},
            {type: articleTYPE.DELETE_ARTICLE_ERROR, message: '删除文章失败'});
}