/**
 * Created by Administrator on 2017/1/4.
 */
import {fromJS} from 'immutable';
import {articleTYPE,subscriptionTYPE} from '../types';

const initState = {};

export default function articleReducer(state = fromJS(initState), action) {
    switch (action.type) {
        case articleTYPE.ARTICLE:
            return state.set(articleTYPE.ARTICLE, fromJS(action.data));

        case articleTYPE.CLEAR_ARTICLE:
            return state.remove(articleTYPE.ARTICLE).remove(articleTYPE.ARTICLE_PAGE);

        case articleTYPE.ARTICLE_PAGE:
            return state.set(articleTYPE.ARTICLE_PAGE, fromJS(action.data));

        case articleTYPE.ARTICLE_LIST:
            return state.set(articleTYPE.ARTICLE_LIST, fromJS(action.data));

        case articleTYPE.CREATE_ARTICLE:
            return state.set(articleTYPE.CREATE_ARTICLE, fromJS(action.data));

        case articleTYPE.UPDATE_ARTICLE:
            return state.set(articleTYPE.UPDATE_ARTICLE, fromJS(action.data));

        default:
            return state
    }
}