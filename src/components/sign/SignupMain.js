/**
 * Created by hldev on 17-2-8.
 */
import React, {Component} from "react";
import {createFormItem} from "../commons/form";
import {Form, Button} from 'antd';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as signService from "../../models/sign/signService";
import * as userService from "../../models/user/userService";
import TYPES from "../../models/types";
import {signTYPE,userTYPE} from '../../models/types';
import Utils from '../../common/Utils';

@connect(
    (state => state),
    dispatch => ({
        signService: bindActionCreators(signService, dispatch),
        userService: bindActionCreators(userService, dispatch),
        TYPES,
        dispatch
    })
)
class SignupMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            passwordDirty: false
        }
    }

    componentWillMount(){
        this.props.userService.findList();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.props.signService.signup(values);
            }
        });
    };

    handlePasswordBlur = (e) => {
        const value = e.target.value;
        this.setState({passwordDirty: this.state.passwordDirty || !!value});
    };

    checkPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('passWord')) {
            callback('您输入的两个密码不一致！');
        } else {
            callback();
        }
    };

    checkConfirm = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.passwordDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    };

    checkUserName = (rule, value, callback) => {
        const userList = Utils.getToObject(this.props.user, userTYPE.USER_LIST);
        const form = this.props.form;
        if (userList.find(user => user.userName === form.getFieldValue('userName'))) {
            callback('用户名已存在！');
        } else {
            callback();
        }
    };

    formControllers = (initialValue = {}) => {
        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        const formList = [{
            type: user.id ? 'static' : 'text',
            item: {label: '用户名:'},
            name: 'userName',
            options: {
                rules: [{required: true, message: '请输入您的用户名！'}, {validator: this.checkUserName}],
                initialValue: user.userName
            }
        }, {
            type: 'radio',
            name: 'role',
            options: {
                rules: [{required: true, message: '请选择您的身份！'}],
                initialValue: user.role + ''
            },
            radioOptions: [{value: '1', label: '老师'}, {value: '2', label: '学生'}]
        }, {
            type: 'text',
            item: {label: '邮箱:'},
            name: 'email',
            options: {
                rules: [{
                    type: 'email', message: '输入的不是有效的电子邮件 ！',
                }, {
                    required: true, message: '请输入您的电子邮件 ！',
                }],
                initialValue: user.email
            }
        }, {
            type: 'text',
            item: {label: '手机号码:'},
            name: 'phone',
            options: {
                rules: [{required: true, message: '请输入您的手机号码！'}],
                initialValue: user.phone
            }
        }, {
            type: 'password',
            item: {label: '密码:'},
            name: 'passWord',
            options: {
                rules: [{
                    required: true, message: '请输入您的密码 ！',
                }, {
                    validator: this.checkConfirm,
                }]
            }
        }, {
            type: 'password',
            item: {label: '确认密码:'},
            name: 'confirm',
            options: {
                rules: [{
                    required: true, message: '请确认您的密码！',
                }, {
                    validator: this.checkPassword,
                }]
            },
            props: {
                onBlur: this.handlePasswordBlur
            }
        }];

        return formList.map((config, key) => createFormItem(this.props.form.getFieldDecorator, config, key))
    };

    render() {
        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    {this.formControllers()}
                    <div style={{textAlign: `center`}}>
                        <Button type="primary" htmlType="submit">
                            {user.id ? '修改' : '注册'}
                        </Button>
                    </div>
                </Form>
            </div>
        )
    }
}

SignupMain = Form.create({})(SignupMain);
export default SignupMain;