/**
 * Created by hldev on 17-2-8.
 */
import React, {Component} from "react"
import {createFormItem} from "../commons/form";
import {Form, Button} from 'antd';

class SigninMain extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // console.log('Received values of form: ', values);
                this.props.signService.signin(values);
            }
        });
    };

    formControllers = (initialValue) => {
        const formList = [{
            type: 'text',
            item: {icon: '用户名'},
            name: 'userName',
        }, {
            type: 'password',
            item: {label: '密码'},
            name: 'passWord',
        }];

        return formList.map((config, key) => createFormItem(this.props.form.getFieldDecorator, config, key))
    };

    render() {
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                {this.formControllers()}
                <div style={{textAlign: `center`}}>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        登陆
                    </Button>
                </div>
            </Form>
        );
    }
}

SigninMain = Form.create({})(SigninMain);
export default SigninMain;