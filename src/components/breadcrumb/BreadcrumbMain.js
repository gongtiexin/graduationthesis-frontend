/**
 * Created by hldev on 17-2-13.
 */
import React, {Component} from "react";
import {Breadcrumb} from 'antd';
import {Link} from "react-router";

export default class BreadcrumbMain extends Component {

    renderBreadcrumb = (obj) => {
        return obj.map((obj, idx) => {
            if (obj.link) {
                return (
                    <Breadcrumb.Item key={idx}>
                        <Link to={obj.link} key={idx} style={{fontSize: '14px'}}>{obj.name}</Link>
                    </Breadcrumb.Item>
                )
            } else return (<Breadcrumb.Item key={idx}>{obj.name}</Breadcrumb.Item>)
        })
    };

    render() {
        return (
            <div style={{margin: '12px 0', borderBottom: '4px solid #404040'}}>
                <Breadcrumb>
                    {this.renderBreadcrumb(this.props.breadcrumb)}
                </Breadcrumb>
            </div>
        )
    }
}