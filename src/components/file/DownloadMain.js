/**
 * Created by hldev on 17-2-15.
 */
import React, {Component} from "react";
import {Link} from "react-router";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import Utils from "../../common/Utils";
import {fileTYPE,signTYPE} from "../../models/types";
import PaginationComponent from "../pagination/PaginationComponent";
import {getParamsFromPage} from '../commons/utils';
import '../../common/style.less';
import SearchComponent from "../commons/SearchComponent"
import constants from "../../common/constants"
import {Card, Button, Col, Row} from "antd"

export default class DownloadMain extends Component {

    componentWillMount() {
        this.props.fileService.findPage();
    }

    renderData = (recordsList) => {
        return recordsList.map((obj, idx) => {
            const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
            return (
                <div style={{background: '#ECECEC', padding: '5px'}} key={idx}>
                    <Card bordered={false}>
                        <Row>
                            <Col span={4}><span>上传人：{obj.createName}</span></Col>
                            <Col span={4}><span>上传时间：{obj.createDate}</span></Col>
                            <Col span={4}><span>摘要：{obj.description}</span></Col>
                            <Col span={8}><a
                                href={`/demo/file/download?path=${obj.filePath}`}>{obj.fileName}</a></Col>
                            <Col span={4}>{user.id === 1 ? <Button type="primary" onClick={() => {this.props.fileService.deleteFile(obj.id);this.props.fileService.findPage(this.props.location.query)}}>删除</Button> : null}</Col>
                </Row>
                    </Card>
                </div>)
        })
    };

    searchControls = (initialValue = {}) => {
        let __date = [];
        if (initialValue.beginDate && initialValue.endDate) {
            __date[0] = moment(initialValue.beginDate, 'YYYY-MM-DD');
            __date[1] = moment(initialValue.endDate, 'YYYY-MM-DD');
        }
        return [
            {
                type: 'text',
                item: {label: '上传用户'},
                name: 'createName',
                options: {
                    initialValue: initialValue.title
                },
            }, {
                type: 'text',
                item: {label: '文件名'},
                name: 'fileName',
                options: {
                    initialValue: initialValue.title
                },
            }, {
                type: 'rangeDatePicker',
                item: {label: '上传时间'},
                name: '__date',
                options: {
                    initialValue: __date
                },
                props: {
                    placeholder: ['开始时间', '结束时间'],
                    format: 'YYYY-MM-DD'
                }
            }];
    };

    handleSearch = (payload) => {
        const query = {...payload};

        if (query.__date) {
            const [beginDate, endDate] = query.__date;
            if (beginDate && endDate) {
                query.beginDate = beginDate.format('YYYY-MM-DD');
                query.endDate = endDate.format('YYYY-MM-DD');
            }
            query.__date = undefined;
        }
        query.page = 1;
        this.props.fileService.findPage({...query}, true);
        Utils.pushLink(this.props.location.pathname, query);

    };

    render() {

        const filePage = Utils.getToJS(this.props.file, fileTYPE.FILE_PAGE, {content: []});

        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: '相关资料',
            }
        ];

        const paginationProps = {
            pageSize: filePage.size || 0,
            currentPage: filePage.page || 0,
            totalCount: filePage.totalElements || 0,
            alwaysShow: true,
            onChange: (page, size) => {
                const params = getParamsFromPage(filePage)(this.props.location.query, {page, size});
                this.props.fileService.findPage(params);
            },
        };

        const searchProps = {
            controls: this.searchControls(this.props.location.query),
            onSearch: this.handleSearch
        };

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                <Card bordered={false}>
                    <div style={{margin: `5px 0`}}>
                        <SearchComponent {...searchProps}/>
                    </div>
                </Card>
                {this.renderData(filePage.content)}
                <PaginationComponent options={paginationProps}/>
            </div>
        )
    }
}