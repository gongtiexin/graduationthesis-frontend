/**
 * Created by hldev on 17-2-15.
 */
import React, {Component} from "react";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import {Upload, Icon, notification, Card} from 'antd';
import {signTYPE} from '../../models/types';
import Utils from '../../common/Utils';
const Dragger = Upload.Dragger;

export default class UploadMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            props: {
                multiple: true,
                showUploadList: false,
                action: '/demo/file',
                data: {
                    description: null,
                    createBy: null,
                    createName: null,
                },
            },
            disabled: true
        }
    }

    setProps = () => {
        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        this.setState({
            props: {
                multiple: true,
                showUploadList: false,
                action: '/demo/file',
                data: {
                    description: this.refs.description.value,
                    createBy: user.id,
                    createName: user.userName,
                },
            },
            disabled: false
        })
    };

    render() {
        const props = {
            ...this.state.props,
            onChange: (info) => {
                const status = info.file.status;
                if (status !== 'uploading') {
                    console.log(info.file, info.fileList);
                }
                if (status === 'done') {
                    notification.success({message: `${info.file.name}文件上传成功`});
                    this.refs.description.value = null
                } else if (status === 'error') {
                    notification.error({message: `${info.file.name}文件上传失败`});
                }
            },
        };

        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: '上传文件',
            }
        ];

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                <div style={{background: '#ECECEC'}}>
                    <Card bordered={false}>
                        <div style={{margin: `5px 0`}}>
                            <p><strong>摘要(必填):</strong></p>
                            <textarea className="ant-input" ref="description" onChange={this.setProps}/>
                        </div>
                    </Card>
                </div>
                <div style={{marginTop: 20, height: 400, marginRight: 100, marginLeft: 100}}>
                    <Dragger {...props} disabled={this.state.disabled}>
                        <p className="ant-upload-drag-icon">
                            <Icon type="inbox"/>
                        </p>
                        <p className="ant-upload-text">请先填写摘要</p>
                        <p className="ant-upload-text">单击或拖动到此区域来上传文件</p>
                        <p className="ant-upload-hint">支持单个或批量上传</p>
                    </Dragger>
                </div>
            </div>
        )
    }
}