/**
 * Created by hldev on 17-2-6.
 */
import React, {Component} from 'react'
import RouteUtils from "../../routes/RouteUtils";
import {Link} from 'react-router';
import '../pagination/style.less';
import {Layout, Menu, Breadcrumb, Icon} from 'antd';
const {Header, Content, Footer, Sider} = Layout;
const SubMenu = Menu.SubMenu;

export default class HomeMain extends Component {
    state = {
        collapsed: false,
        mode: 'inline',
    };
    onCollapse = (collapsed) => {
        console.log(collapsed);
        this.setState({
            collapsed,
            mode: collapsed ? 'vertical' : 'inline',
        });
    };

    renderMenu = () => {
        return RouteUtils.globalMenus.map(item => {
            return (
                <Menu.Item key={item.link}>
                    <Link to={item.link}>
              <span>
                <Icon type={item.iconType}/>
                <span className="nav-text">{item.title}</span>
              </span>
                    </Link>
                </Menu.Item>
            )
        })
    };

    render() {
        return (
            <Layout>
                <Sider
                    collapsible
                    collapsed={this.state.collapsed}
                    onCollapse={this.onCollapse}
                    id="sider"
                >
                    <div className="logo"/>
                    <Menu theme="dark" mode={this.state.mode} defaultSelectedKeys={['6']}>
                        {this.renderMenu()}
                    </Menu>
                </Sider>
                <Layout>
                    {/*<Header style={{background: '#fff', padding: 0}} id="header">*/}
                    {/*</Header>*/}
                    <Content style={{margin: '0 16px'}} id="content">
                        <div style={{padding: 24}}>
                            {this.props.children}
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}} id="footer">
                        Graduation Thesis ©2016 Created by Ant UED
                    </Footer>
                </Layout>
            </Layout>
        );
    }
}