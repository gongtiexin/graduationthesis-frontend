/**
 * Created by hldev on 17-2-7.
 */
import React, {Component} from "react";
import ReactEchart from "../xecharts/ReactEchart";
import Immutable from "immutable";
import Utils from "../../common/Utils"
import {signTYPE} from '../../models/types';
import SignupMain from "../sign/SignupMain";
import SigninMain from "../sign/SigninMain";
import UnreadRecordsMain from "../records/UnreadRecordsMain"
import UserMain from "../user/UserMain"
import {Row, Col, Tabs, notification, Card} from "antd";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as TYPES from "../../models/types";
import * as signService from "../../models/sign/signService"
import * as recordsService from "../../models/records/recordsService"
import * as articleService from "../../models/article/articleService"
import * as fileService from "../../models/file/fileService"
const TabPane = Tabs.TabPane;
import moment from "moment"
import lodashGroupBy from "lodash/groupBy";

@connect(
    (state => state),
    dispatch => ({
        signService: bindActionCreators(signService, dispatch),
        recordsService: bindActionCreators(recordsService, dispatch),
        articleService: bindActionCreators(articleService, dispatch),
        fileService: bindActionCreators(fileService, dispatch),
        TYPES,
        dispatch
    })
)
export default class IndexMain extends Component {

    componentWillMount() {
        this.props.articleService.findList();
        this.props.recordsService.findList();
        this.props.fileService.findList();
    }

    option = () => {
        const data = [];
        const articleList = Utils.getToArray(this.props.article, TYPES.articleTYPE.ARTICLE_LIST);
        const recordsList = Utils.getToArray(this.props.records, TYPES.recordsTYPE.RECORDS_LIST);
        const fileList = Utils.getToArray(this.props.file, TYPES.fileTYPE.FILE_LIST);
        articleList.map(t => {
            t.week = moment(t.createDate).format('d')
        });
        recordsList.map(t => {
            t.week = moment(t.createDate).format('d')
        });
        fileList.map(t => {
            t.week = moment(t.createDate).format('d')
        });

        const articleListGroup = lodashGroupBy(articleList,"week");
        for (const week of Object.keys(articleListGroup)) {
            for(let i = 0;i<= 3;i++){
                data.push([week,i,articleListGroup[week].filter(obj => obj.category === i).length]);
            }
        }

        const recordsListGroup = lodashGroupBy(articleList,"week");
        for (const week of Object.keys(recordsListGroup)) {
                data.push([week,4,recordsListGroup[week].length]);
        }

        const fileListGroup = lodashGroupBy(articleList,"week");
        for (const week of Object.keys(fileListGroup)) {
            data.push([week,5,fileListGroup[week].length]);
        }

        const week = ['周一', '周二', '周三', '周四', '周五', '周六', '周末'];
        const category = ['发展历史', '教学内容', '教师团队',
            '教学成果', '文件总量', '论坛总量'];
        console.log(data);

        return {
            style: {
                width: '100%',
                textAlign: "center",
                height: '500px',
            },
            option: Immutable.fromJS(
                {
                    tooltip: {},
                    visualMap: {
                        max: 20,
                        inRange: {
                            color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
                        }
                    },
                    xAxis3D: {
                        type: 'category',
                        data: week
                    },
                    yAxis3D: {
                        type: 'category',
                        data: category
                    },
                    zAxis3D: {
                        type: 'value'
                    },
                    grid3D: {
                        boxWidth: 200,
                        boxDepth: 80,
                        light: {
                            main: {
                                intensity: 1.2
                            },
                            ambient: {
                                intensity: 0.3
                            }
                        },
                        viewControl:{
                            distance: 200
                        }
                    },
                    series: [{
                        type: 'bar3D',
                        data: data,
                        shading: 'color',

                        label: {
                            show: false,
                            textStyle: {
                                fontSize: 16,
                                borderWidth: 1
                            }
                        },

                        itemStyle: {
                            opacity: 0.4
                        },

                        emphasis: {
                            label: {
                                textStyle: {
                                    fontSize: 20,
                                    color: '#900'
                                }
                            },
                            itemStyle: {
                                color: '#900'
                            }
                        }
                    }]
                }
            )
        }
    };

    renderLeftModal = () => {
        const signUser = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        if (signUser.id) {
            // notification.success({message:'登陆成功'});
            return (
                <div>
                    <UserMain signUser={signUser} signService={this.props.signService}/>
                    <Card title="未读消息" style={{marginTop: 20}}>
                        <UnreadRecordsMain recordsService={this.props.recordsService}
                                           records={this.props.records}
                                           signUser={signUser}/>
                    </Card>
                </div>
            )
        } else return (
            <Tabs defaultActiveKey="2">
                <TabPane tab="登陆" key="1">
                    <SigninMain signService={this.props.signService}/>
                </TabPane>
                <TabPane tab="注册" key="2">
                    <SignupMain signService={this.props.signService}/>
                </TabPane>
            </Tabs>
        )


    };

    render() {
        const breadcrumb = [
            {
                name: '首页',
            }
        ];

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                <Row gutter={24}>
                    <Col span={15}>
                        <div style={{ background: '#ECECEC'}}>
                            <Card title="课程网站信息统计" bordered={false}>
                                <ReactEchart option={this.option()}/>
                            </Card>
                        </div>
                    </Col>
                    <Col span={9}>
                        <div style={{ background: '#ECECEC'}}>
                            <Card bordered={false}>
                                {this.renderLeftModal()}
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}