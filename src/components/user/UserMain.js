/**
 * Created by hldev on 17-2-4.
 */
import React, {Component} from 'react';
import {Row, Col, Form, Icon, Card, Button} from "antd";
import moment from 'moment';
import SignupMain from "../sign/SignupMain"

class UserMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: 0
        }
    }

    formControllers = (initialValue) => {
        const roleMap = {'1': '老师', '2': '学生'};
        const formList = [{
            key: '身份',
            value: roleMap[initialValue.role]
        }, {
            key: '电子邮箱',
            value: initialValue.email
        }, {
            key: '手机号码',
            value: initialValue.phone
        }];

        if (this.state.status === 0) {
            return formList.map(obj => {
                return (
                    <p key={obj.value} style={{margin: `20px 0`}}>{obj.key} : {obj.value}</p>
                )
            })
        } else return (
            <SignupMain/>
        )
    };

    changeStatus = () => {
        if (this.state.status === 0) {
            this.setState({status: 1})
        } else if (this.state.status === 1) {
            this.setState({status: 0})
        }
    };

    render() {
        return (
            <div style={{marginTop: 10}}>
                <Card title={<div>
                    {moment().format('YYYY年MM月DD日 dddd')}&nbsp;&nbsp;&nbsp;<Icon
                    type="user"/>{this.props.signUser.userName}
                    <Row type="flex" justify="end">
                        <Col span={6}>
                            <Button type="primary"
                                    onClick={this.changeStatus}>{this.state.status === 0 ? '编辑' : '返回'}</Button>
                        </Col>
                        <Col span={6}>
                            <Button type="primary"
                                    onClick={() => this.props.signService.logout()}>{this.props.signUser ? '登出' : null}</Button>
                        </Col>
                    </Row>
                </div>} bordered={false}>
                    {this.formControllers(this.props.signUser)}
                </Card>
            </div>
        )
    }
}

UserMain = Form.create({})(UserMain);
export default UserMain;