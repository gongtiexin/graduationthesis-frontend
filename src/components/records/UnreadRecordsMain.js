/**
 * Created by hldev on 17-2-16.
 */
import React, {Component} from "react";
import {recordsTYPE} from "../../models/types";
import Utils from "../../common/Utils";
import moment from "moment";
import {Link} from "react-router";
import {Button, Card, Table, Row, Col} from "antd";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import PaginationComponent from "../pagination/PaginationComponent";
import {getParamsFromPage} from '../commons/utils';
import SearchComponent from "../commons/SearchComponent"

export default class UnreadRecordsMain extends Component {

    componentWillMount() {
        this.props.recordsService.findPage({status: 0, parentCreateBy: this.props.signUser.id});
    }

    tableColumns = () => {
        return [{
            title: '回复人',
            dataIndex: 'createName',
            key: 'createName',
        }, {
            title: '回复时间',
            dataIndex: 'createDate',
            key: 'createDate',
        }, {
            title: '详情',
            dataIndex: 'x',
            key: 'x',
            render: (text, record, query) => {
                return (
                    <Link to={`/system/records/detail/${record.themeId}`}
                          query={{id: record.id, themeId: record.themeId}}>
                        详情
                    </Link>
                )
            }
        }];
    };

    tableDataSource = (content) => {
        return content.map((item, idx) => {
            return {
                ...item,
                key: idx
            }
        })
    };

    render() {
        const unreadPage = Utils.getToJS(this.props.records, recordsTYPE.RECORDS_PAGE, {content: []});
        const tableColumns = this.tableColumns();
        const tableDataSource = this.tableDataSource(unreadPage.content);

        const paginationProps = {
            pageSize: unreadPage.size || 0,
            currentPage: unreadPage.page || 0,
            totalCount: unreadPage.totalElements || 0,
            alwaysShow: true,
            onChange: (page, size) => {
                const params = getParamsFromPage(unreadPage)(this.props.location ? this.props.location.query : {}, {
                    page,
                    size
                });
                this.props.recordsService.findUnread(params);
            },
        };

        return (
            <div>
                <Table bordered
                       size="small"
                       columns={tableColumns}
                       dataSource={tableDataSource}
                       pagination={false}/>
                <PaginationComponent options={paginationProps}/>
            </div>
        )
    }
}