/**
 * Created by hldev on 17-2-13.
 */
import React, {Component} from 'react';
import {Button, notification} from 'antd';
import {recordsTYPE, signTYPE} from '../../models/types';
import Utils from '../../common/Utils';
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"

export default class RecordsEditor extends Component {

    editor = null;

    componentDidMount() {
        this.editor = UE.getEditor('editor', {
            initialFrameWidth: '100%',initialFrameHeight: 320, autoHeightEnabled: false, toolbars: [[
                'fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                'simpleupload', 'insertimage', 'attachment', '|',
                'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
                'print', 'preview', 'searchreplace', 'drafts', 'help'
            ]]
        });
    }

    componentWillUnmount() {
        if (this.editor) {
            this.editor.destroy();
        }
        this.props.recordsService.clearRecords();
    }

    isAdd = () => this.props.params.id === 'add';

    handleClick = () => {
        const themeRecord = Utils.getToObject(this.props.records, recordsTYPE.RECORDS);
        const content = this.editor.getContent();
        if (!content) {
            notification.warn({message: '您未编辑正文'});
            return;
        }

        if (this.refs.title && !this.refs.title.value) {
            notification.warn({message: '您未编辑标题'});
            return;
        }

        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);

        const records = {
            ...Utils.getToObject(this.props.records, recordsTYPE.RECORDS),
            content: content,
            title: this.refs.title ? this.refs.title.value : null,
            parentRecordId: this.isAdd() ? 0 : this.props.params.id,
            parentCreateBy: this.props.location.query.parentCreateBy ? this.props.location.query.parentCreateBy : 0,
            themeId: themeRecord ? themeRecord.id : null,
            status: 0,
            createBy: user.id,
            createName: user.userName
        };
        this.props.recordsService.createRecords(records);
        if (this.isAdd()) {
            Utils.pushLink('/system/records');
        } else {
            const id = this.props.location.query.parentRecordId ? this.props.location.query.parentRecordId : this.props.params.id;
            const path = '/system/records/detail/' + id;
            Utils.pushLink(path);
        }
    };

    renderTitle = () => {
        if (this.isAdd()) {
            return (
                <div>
                    <p><strong>标题</strong></p>
                    <input className="ant-input" ref="title" style={{margin: '20px 0'}}/>
                    <div className="line"></div>
                </div>
            )
        }
    };

    render() {
        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: '在线论坛',
                link: '/system/records',
            }, {
                name: this.isAdd() ? '发表主题' : '回复主题'
            }
        ];

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                {this.renderTitle()}
                <div>
                    <script id='editor' name="content" type="text/plain"></script>
                </div>
                <div>
                    <Button type="primary" style={{margin: '16px 10px'}}
                            onClick={this.handleClick}>{this.isAdd() ? '创建' : '回复'}</Button>
                </div>
            </div>
        )
    }
}