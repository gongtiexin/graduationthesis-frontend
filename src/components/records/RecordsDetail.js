/**
 * Created by hldev on 17-2-13.
 */
import React, {Component} from "react";
import Utils from "../../common/Utils";
import {recordsTYPE, signTYPE} from "../../models/types";
import {Link} from "react-router";
import {Card, Icon, Button, Row, Col, Table, Collapse} from "antd";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import PaginationComponent from "../pagination/PaginationComponent";
import {getParamsFromPage} from '../commons/utils';
const Panel = Collapse.Panel;

export default class RecordsDetail extends Component {

    componentWillMount() {
        this.props.recordsService.findPage({
            parentRecordId: this.props.params.id,
            themeId: this.props.location.query.themeId
        });
        this.props.recordsService.findById(this.props.params.id);
        this.props.recordsService.findList();
        if (this.props.location.query.id) {
            this.props.recordsService.setStatus({id: this.props.location.query.id, status: 1});
        }
    }

    componentWillUnmount() {
        this.props.recordsService.clearRecords()
    }

    renderChild = (obj, recordList, user) => {
        return recordList.map((item, idx) => {
            if (item.parentCreateBy == obj.createBy && item.parentRecordId == obj.id) {
                return (
                    <div style={{padding: '5px', color: '#000000'}} key={idx}>
                        {<span>{`回复人：${item.createName}`}<span
                            style={{marginLeft: 20}}/>{`回复时间：${item.createDate}`}</span>}
                        <div className="content"
                             style={(this.props.location.query.id && item.id == this.props.location.query.id) ? {
                                 color: '#FF4949',
                                 padding: '5px'
                             } : {color: '#000000', padding: '5px'}}>
                            <Row>
                                <Col span={20}>
                                    <div dangerouslySetInnerHTML={{__html: item.content}}>
                                    </div>
                                </Col>
                                <Col span={4}>
                                    {user.id === 1 ? <Button type="primary" onClick={() => {
                                        this.props.recordsService.deleteRecords(item.id);
                                        this.props.recordsService.findList();
                                        this.props.recordsService.findPage({
                                            parentRecordId: this.props.params.id,
                                            themeId: this.props.location.query.themeId
                                        })
                                    }}>删除</Button> : null}
                                </Col>
                            </Row>
                        </div>
                    </div>
                )
            }
        })
    };

    renderData = (content, recordList) => {
        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        return content.map((obj, idx) => {
            return (
                <div style={{padding: '5px'}} key={idx}>
                    <Card
                        title={
                            <span>{`回复人：${obj.createName}`}
                                <span style={{marginLeft: 20}}/>
                                {`回复时间：${obj.createDate}`}
                            </span>}
                        bordered={false}
                        extra={<Link to={`/system/records/editor/${obj.id}`} query={{
                            parentCreateBy: obj.createBy,
                            parentRecordId: obj.parentRecordId
                        }}>回复</Link>}>
                        <div className="content"
                             style={(this.props.location.query.id && obj.id == this.props.location.query.id) ? {
                                 color: '#FF4949',
                                 padding: '5px',
                                 borderBottom: '1px solid #404040',
                             } : {color: '#000000', padding: '5px', borderBottom: '1px solid #404040'}}>
                            <Row>
                                <Col span={20}>
                                    <div dangerouslySetInnerHTML={{__html: obj.content}}>
                                    </div>
                                </Col>
                                <Col span={4}>
                                    {user.id === 1 ? <Button type="primary" onClick={() => {
                                        this.props.recordsService.deleteRecords(obj.id);
                                        this.props.recordsService.findPage({
                                            parentRecordId: this.props.params.id,
                                            themeId: this.props.location.query.themeId
                                        })
                                    }}>删除</Button> : null}
                                </Col>
                            </Row>
                        </div>
                        {this.renderChild(obj, recordList, user)}
                    </Card>
                </div>
            )
        })
    };

    render() {

        const recordsPage = Utils.getToJS(this.props.records, recordsTYPE.RECORDS_PAGE, {content: []});
        const records = Utils.getToObject(this.props.records, recordsTYPE.RECORDS);
        const recordList = Utils.getToArray(this.props.records, recordsTYPE.RECORDS_LIST);

        const paginationProps = {
            pageSize: recordsPage.size || 0,
            currentPage: recordsPage.page || 0,
            totalCount: recordsPage.totalElements || 0,
            alwaysShow: true,
            onChange: (page, size) => {
                const params = getParamsFromPage(recordsPage)(this.props.location.query, {page, size});
                this.props.recordsService.findPage({...params, parentRecordId: this.props.params.id});
            },
        };

        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: '在线论坛',
                link: '/system/records',
            }, {
                name: records.title
            }
        ];

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                <div style={{margin: 10}}>
                    <h1 style={{color: '#3398DB'}}>{`主题：${records.title}`}</h1>
                </div>
                <div style={{margin: 10}}>
                    <h2 style={{color: '#3398DB'}}>
                        <div dangerouslySetInnerHTML={{__html: records.content}}/>
                    </h2>
                </div>
                {this.renderData(recordsPage.content, recordList)}
                <Row>
                    <Col span={2}>
                        <Link to={`/system/records/editor/${records.id}`} query={{parentCreateBy: records.createBy}}>
                            <Button style={{margin: '16px 10px'}} type="primary">回复</Button>
                        </Link></Col>
                    <Col span={22}>
                        <PaginationComponent options={paginationProps}/>
                    </Col>
                </Row>
            </div>

        )
    }
}