/**
 * Created by hldev on 17-2-9.
 */
import React, {Component} from "react"
import {recordsTYPE,signTYPE} from "../../models/types";
import Utils from "../../common/Utils";
import moment from "moment";
import {Link} from "react-router";
import {Button, Card, Table, Row, Col} from "antd";
import BreadcrumbMain from "../breadcrumb/BreadcrumbMain"
import PaginationComponent from "../pagination/PaginationComponent";
import {getParamsFromPage} from '../commons/utils';
import '../../common/style.less';
import SearchComponent from "../commons/SearchComponent"

export default class RecordsMain extends Component {

    componentWillMount() {
        this.props.recordsService.findPage({parentRecordId: 0});
    }

    componentDidMount() {
        this.props.recordsService.clearRecords();
    }

    renderData = (recordsList) => {
        const user = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        return recordsList.map((obj, idx) => {
            return (
                <div style={{background: '#ECECEC', padding: '5px'}} key={idx}>
                    <Card bordered={false}>
                        <Row>
                            <Col span={12}>
                                <Link to={`/system/records/detail/${obj.id}`} query={{themeId: obj.id}}>
                                    <h2>{obj.title}</h2>
                                </Link></Col>
                            <Col span={4}><span>发表人：{obj.createName}</span></Col>
                            <Col span={4}><span>{moment(obj.createDate).format('YYYY-MM-DD')}</span></Col>
                            <Col span={4}>{user.id === 1 ? <Button type="primary" onClick={() => {this.props.recordsService.deleteRecords(obj.id);this.props.recordsService.findPage({parentRecordId: 0,...this.props.location.query})}}>删除</Button> : null}</Col>
                        </Row>
                    </Card>
                </div>)
        })
    };

    searchControls = (initialValue = {}) => {
        let __date = [];
        if (initialValue.beginDate && initialValue.endDate) {
            __date[0] = moment(initialValue.beginDate, 'YYYY-MM-DD');
            __date[1] = moment(initialValue.endDate, 'YYYY-MM-DD');
        }
        return [{
            type: 'text',
            item: {label: '主题'},
            name: 'title',
            options: {
                initialValue: initialValue.title
            },
        }, {
            type: 'text',
            item: {label: '发表人'},
            name: 'createName',
            options: {
                initialValue: initialValue.title
            },
        }, {
            type: 'rangeDatePicker',
            item: {label: '发表时间'},
            name: '__date',
            options: {
                initialValue: __date
            },
            props: {
                placeholder: ['开始时间', '结束时间'],
                format: 'YYYY-MM-DD'
            }
        }];
    };

    handleSearch = (payload) => {
        const query = {...payload};

        if (query.__date) {
            const [beginDate, endDate] = query.__date;
            if (beginDate && endDate) {
                query.beginDate = beginDate.format('YYYY-MM-DD');
                query.endDate = endDate.format('YYYY-MM-DD');
            }
            query.__date = undefined;
        }
        query.page = 1;
        this.props.recordsService.findPage({...query, parentRecordId: 0}, true);
        Utils.pushLink(this.props.location.pathname, query);

    };

    render() {
        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: '在线论坛',
            }
        ];

        const recordsPage = Utils.getToJS(this.props.records, recordsTYPE.RECORDS_PAGE, {content: []});

        const paginationProps = {
            pageSize: recordsPage.size || 0,
            currentPage: recordsPage.page || 0,
            totalCount: recordsPage.totalElements || 0,
            alwaysShow: true,
            onChange: (page, size) => {
                const params = getParamsFromPage(recordsPage)(this.props.location.query, {page, size});
                this.props.recordsService.findPage({...params, parentRecordId: 0});
            },
        };

        const searchProps = {
            controls: this.searchControls(this.props.location.query),
            onSearch: this.handleSearch
        };

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                <div style={{background: '#ECECEC'}}>
                    <Card bordered={false}>
                        <SearchComponent {...searchProps}/>
                    </Card>
                </div>
                {this.renderData(recordsPage.content)}
                <Row>
                    <Col span={2}>
                        <Link to="/system/records/editor/add">
                            <Button style={{margin: '16px 10px'}} type="primary">发表主题</Button>
                        </Link></Col>
                    <Col span={22}><PaginationComponent options={paginationProps}/></Col>
                </Row>

            </div>
        )
    }
}