/**
 * React Echarts Component
 * Created by yangbajing(yangbajing@gmail.com) on 2016-09-18.
 */
import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import echarts from "echarts";
import echartsGl from "echarts-gl";

export default class ReactEchart extends Component {
    static propTypes = {
        // 传入option需要为Immutable对像实例 
        option: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            isReset: false
        };
        this.myChart = null;
    }

    componentDidMount() {
        this.myChart = echarts.init(ReactDOM.findDOMNode(this.refs.myChart));
        if (this.props.onClick) {
            this.myChart.on('click', this.props.onClick)
        }
        if (!this.state.isReset) {
            this.setState(Object.assign({}, this.state, {isReset: true}));
        }
    }

    componentWillUnmount() {
        this.myChart.dispose();
    }

    // componentWillReceiveProps(nextProps) {
    // console.info('nextProps', nextProps);
    // }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps !== this.props || nextState !== this.state;
    }

    render() {
        if (this.props.option && this.myChart) {
            const option = this.props.option.option.toJS();
            if (option) {
                this.myChart.setOption(option);
            }
        }
        return (
            <div ref="myChart" style={this.props.option.style}>
            </div>
        )
    }
}
