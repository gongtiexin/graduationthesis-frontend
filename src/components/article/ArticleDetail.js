/**
 * Created by hldev on 17-2-14.
 */
import React, {Component, PropTypes} from "react";
import moment from "moment";
import {Spin, Button, Icon} from 'antd';
import {Link} from 'react-router';
import lodashSortBy from 'lodash/sortBy';
import Utils from '../../common/Utils';
import {articleTYPE, signTYPE} from "../../models/types";
import BreadcrumbMain from '../breadcrumb/BreadcrumbMain';

export default class ArticleDetail extends Component {

    componentWillMount() {
        this.props.articleService.findById(this.props.params.id);

    }

    componentWillUnmount() {
        this.props.articleService.clearArticle();
    }

    createElement = (article) => {
        const signUser = Utils.getToObject(this.props.sign, signTYPE.SIGN_IN);
        if (signUser.id === article.createBy || signUser.id === 1) {
            return (
                <div className="text-right">
                    <Link to={`/system/about/editor/${article.id}`} query={{category: article.category}}>
                        <Button style={{float: 'right', margin: '16px 10px'}} type="primary"><Icon
                            type="edit"/>编辑</Button>
                    </Link>
                </div>
            )
        }
    };

    render() {

        const article = Utils.getToObject(this.props.article, articleTYPE.ARTICLE);

        const menuMap = {'1': '发展历史', '2': '教学内容', '3': '教师团队', '4': '教学成果'};

        const breadcrumb = [
            {
                name: '首页',
                link: '/'
            }, {
                name: menuMap[article.category],
                link: '/system/about/' + article.category
            }, {
                name: article.title
            }
        ];

        return (
            <div>
                <BreadcrumbMain breadcrumb={breadcrumb}/>
                {this.createElement(article)}
                <div style={{textAlign: 'center', margin: `16px 0`}}>
                    <h1>{article.title}</h1>
                </div>
                <div style={{textAlign: 'center', margin: `8px 0`}}>
                    <span>{moment(article.createDate).format('YYYY-MM-DD HH:mm')}</span>
                </div>
                <div style={{border: `2px solid #ececec`, margin: `0 100px`, padding: 50, fontSize: '20px'}}>
                    <div dangerouslySetInnerHTML={{__html: article.content}}>
                    </div>
                </div>
            </div>
        );
    }
}
