/**
 * 分页组件
 * Created by yangbajing(yangbajing@gmail.com) on 2016-11-15.
 */
import './style.less';
import React, {Component, PropTypes} from 'react';
import {Pagination} from 'antd';
import Constants from '../../common/constants';

const defaultProps = {
    showSizeChanger: true,
    showQuickJumper: true,
    showMessage: true
};

class PaginationComponent extends Component {
    static propTypes = {
        /**
         * pageSize: int = 每页显示记录条数
         * currentPage: int = 当前页码
         * totalCount: int = 总记录条数
         */
        options: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            pageSize: props.options.pageSize || Constants.DEFAULT_PAGE.size
        };
    }

    handleChange = (currentPage, selectPageSize) => {
        let pageSize = selectPageSize;
        if (pageSize) {
            this.setState({pageSize});
            currentPage = 1;
        } else {
            pageSize = this.state.pageSize || Constants.DEFAULT_PAGE.size;
        }

        if (this.props.options.onChange) {
            this.props.options.onChange(currentPage, pageSize);
        }
    };

    render() {
        const options = {...defaultProps, ...this.props.options};

        const pageSize = options.pageSize || Constants.DEFAULT_PAGE.size;
        const currentPage = options.currentPage || Constants.DEFAULT_PAGE.page;
        const totalCount = options.totalCount;
        const showSizeChanger = options.showSizeChanger;
        const showQuickJumper = options.showQuickJumper;
        const props = {};
        if (showSizeChanger) {
            props.showSizeChanger = true;
        }
        if (showQuickJumper) {
            props.showQuickJumper = true;
        }
        const totalPage = Math.ceil(totalCount / pageSize);
        let style = this.props.style;
        //if (totalPage <= 1) {
        //    style = assign({}, {display: 'none'}, style);
        //}
        return (
            <div className="pagination-component" style={style}>
                <div className="pagination-wrap">
                    <Pagination
                        {...props}
                        onShowSizeChange={this.handleChange}
                        onChange={this.handleChange}
                        defaultCurrent={1}
                        pageSize={pageSize}
                        current={currentPage}
                        total={totalCount}
                        showTotal={(totalCount) => `共 ${totalCount} 条`}
                    />
                </div>
                {/*{this.props.children}*/}
                <div style={{clear: 'both'}}></div>
            </div>
        );
    }
}

export default PaginationComponent;
