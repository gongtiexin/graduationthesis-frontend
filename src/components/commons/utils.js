/**
 * 工具
 * Created by yangbajing(yangbajing@gmail.com) on 2016-11-1.
 */
import lodashKeys from "lodash/keys";
import {Iterable} from "immutable";
import Constants from "../../common/constants";
import Utils from "../../common/Utils";

export function makeOpenKeys(current) {
    if (!current) {
        return [];
    }

    const keys = current.split('/').filter(v => v !== '');
    let openKeys = [current];
    for (let i = keys.length - 1; i > 0; i--) {
        const p = keys.slice(0, i).join('/');
        openKeys.push('/' + p);
        openKeys.push(p);
    }
    return openKeys;
}

export function humanUserSex(sex) {
    return Constants.USER.SEX[sex];
}

export function humanUserStatus(status) {
    return Constants.USER.STATUS[status];
}

/**
 * 根据state中的XXX_PAGE对象来获取分页参数
 * @param page state中取出的 XXX_PAGE 对象，兼容Immutable对象和JS原始对象
 * @param paramNames 可选的参数名
 * @return {*}
 */
export function parseParamsFromPage(page, ...paramNames) {
    if (!page)
        return {...Constants.DEFAULT_PAGE};
    let params = {};

    if (Iterable.isIterable(page)) {
        params.page = page.has('number') ? page.get('number') + 1 : Constants.DEFAULT_PAGE.page;
        params.size = page.has('size') ? page.get('size') : Constants.DEFAULT_PAGE.size;
        for (name of paramNames) {
            if (page.has(name))
                params[name] = page.get(name);
        }
    } else {
        params.page = page.number !== undefined && page.number !== null ? page.number + 1 : Constants.DEFAULT_PAGE.page;
        params.size = page.size || Constants.DEFAULT_PAGE.size;
        for (name of paramNames) {
            if (page[name])
                params[name] = page[name];
        }
    }

    return clearBlankKeys(params);
}

/**
 * getParamsFromPage(summaryXzcfPage)(this.props.location.query)
 * @param page XXX_PAGE 对象
 * @param paramNames 可先参数名
 * @return {function(*, *=)}
 */
export function getParamsFromPage(page, ...paramNames) {
    /**
     * @param query HTTP查询参数，一般从 this.props.location.query 获取
     * @param extQuery 附加的参数参数对象
     * @return 返回有效的查询参数对象
     */
    return (query, extQuery = {}) => {
        let params = {...parseParamsFromPage(page, ...paramNames), ...query, ...extQuery};
        params = clearBlankKeys(params);
        return Utils.transAttributesToNumber(params, ['page', 'size']);
    }
}

export function clearBlankKeys(payload) {
    let value = {};
    for (const key of lodashKeys(payload)) {
        if (payload[key]) {
            value[key] = payload[key]
        }
    }
    return value;
}

/**
 * 生成基本Ant.Design Pagination Props对象
 *
 * @param pageObject
 * @param query
 * @param path
 * @return {{total: *, className: string, current: *, pageSize: *, onChange: (function(*))}}
 */
export function generatePaginationProps(pageObject, query, path) {
    const queryParams = getParamsFromPage(pageObject)(query);

    let total;
    if (Iterable.isIterable(pageObject)) {
        total = pageObject.get('totalElements') || 0;
    } else {
        total = pageObject.totalElements || 0;
    }

    return {
        total,
        className: 'ant-table-pagination',
        current: queryParams.page,
        pageSize: queryParams.size,
        onChange: (page) => Utils.pushLink(path, {...queryParams, page})
    };
}
