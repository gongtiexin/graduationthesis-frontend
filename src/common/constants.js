/**
 * Define Action Types
 * Created by yangbajing(yangbajing@gmail.com) on 2016-08-19.
 */
const Constants = {
    LOCALHOST: 'http://demo.test',
    BASE_CONSOLE_URL: '',
    DEFAULT_PAGE: {page: 1, size: 10},
    USER: {
        SEX: {
            F: '女',
            M: '男',
            U: '未知'
        },
        STATUS: {
            '0': '禁用',
            '1': '启用',
        }
    },
    ADMIN: '1'
};


export default Constants;