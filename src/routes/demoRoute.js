/**
 * Created by hldev on 17-2-9.
 */
/**
 * 路由配置
 * Created by Yang Jing (yangbajing@gmail.com) on 2016-08-16.
 */
import React, {Component} from "react";
import {Router, Route, IndexRoute, browserHistory} from "react-router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {signInAuthInterceptor} from './RouteUtils';
import * as TYPES from "../models/types";
import RecordsMain from "../components/records/RecordsMain";
import RecordsEditor from "../components/records/RecordsEditor";
import RecordsDetail from "../components/records/RecordsDetail";
import ArticleMain from "../components/article/ArticleMain";
import ArticleDetail from "../components/article/ArticleDetail";
import ArticleEditor from "../components/article/ArticleEditor";
import UploadMain from "../components/file/UploadMain";
import DownloadMain from "../components/file/DownloadMain";
import * as userService from "../models/user/userService";
import * as signService from "../models/sign/signService";
import * as recordsService from "../models/records/recordsService";
import * as articleService from "../models/article/articleService";
import * as fileService from "../models/file/fileService";

@connect(
    (state => state),
    dispatch => ({
        userService: bindActionCreators(userService, dispatch),
        signService: bindActionCreators(signService, dispatch),
        recordsService: bindActionCreators(recordsService, dispatch),
        articleService: bindActionCreators(articleService, dispatch),
        fileService: bindActionCreators(fileService, dispatch),
        TYPES,
        dispatch
    })
)
class DemoContainer extends Component {
    render() {
        return React.cloneElement(this.props.children, {TYPES, ...this.props});
    }
}

const demoRoute = (
        <Route path="system" onEnter={signInAuthInterceptor} name="毕业设计" component={DemoContainer}>
            <Route path="records" name="在线论坛" component={RecordsMain}/>
            <Route path="records/editor/:id" name="编辑" component={RecordsEditor}/>
            <Route path="records/detail/:id" name="详细" component={RecordsDetail}/>
            <Route path="download" name="相关资料" component={DownloadMain}/>
            <Route path="upload" name="上传文件" component={UploadMain}/>
            <Route path="about/:category" name="关于课程" component={ArticleMain}/>
            <Route path="about/detail/:id" name="课程详细" component={ArticleDetail}/>
            <Route path="about/editor/:id" name="编辑" component={ArticleEditor}/>
        </Route>
);

export default demoRoute;