import store from "../models/store";
import Utils from "../common/Utils";
import {Modal} from "antd";
import {signTYPE} from "../models/types";

const RouteUtils = {
    globalMenus: [{
        title: '课程主页',
        link: '/',
        iconType: 'home'
    },{
        title: '在线论坛',
        link: '/system/records',
        iconType: 'team'
    },{
        title: '相关资料',
        link: '/system/download',
        iconType: 'download'
    },{
        title: '上传文件',
        link: '/system/upload',
        iconType: 'upload'
    },{
        title: '关于课程',
        link: '/system/about/1',
        iconType: 'solution'
    }]

};

export default RouteUtils;

export function signInAuthInterceptor(next, replace, cb) {
    const state = store.getState();
    const user = Utils.getToObject(state.sign,signTYPE.SIGN_IN);
    if (!user.id) {
        Modal.warn({title: '请登录'});
        replace('/');
        cb();
    } else {
        cb();
    }
}
