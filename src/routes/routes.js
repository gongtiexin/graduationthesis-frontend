/**
 * 路由配置
 * Created by Yang Jing (yangbajing@gmail.com) on 2016-08-16.
 */
import React, {Component} from "react";
import {Router, Route, IndexRoute, browserHistory} from "react-router";
import demoRoute from "./demoRoute";
import HomeMain from "../components/home/HomeMain";
import IndexMain from "../components/home/IndexMain";

const routes = (
    <Router history={browserHistory}>
        <Route path="/" name="首页" component={HomeMain}>
            <IndexRoute component={IndexMain}/>
            {demoRoute}
        </Route>
    </Router>
);

export default routes;