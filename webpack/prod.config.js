require('babel-polyfill');
var UglifyJsPlugin = require("webpack/lib/optimize/UglifyJsPlugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var basePath = path.resolve(__dirname, '..');

var commonConfig = require('./common');
commonConfig.plugins = commonConfig.plugins.concat([
    new UglifyJsPlugin({
        output: {
            comments: false,
        },
        compress: {
            warnings: false
        }
    }),
    // new HtmlWebpackPlugin({
    //     filename: basePath + '/dist/site/index.html',
    //     template: basePath + '/src/site/index.html',
    //     chunks: ['site']
    // }),
    new HtmlWebpackPlugin({
        filename: basePath + '/dist/index.html',
        template: basePath + '/src/entries/index.html',
        hash: true,    //为静态资源生成hash值
        minify: {    //压缩HTML文件
            // collapseWhitespace: true,    //删除空白符与换行符
            removeComments: true    //移除HTML中的注释
        },
        chunks: ['commons', 'react', 'console']
    })
]);
module.exports = commonConfig;
