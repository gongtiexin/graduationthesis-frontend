/**
 * Created by yangbajing on 2016-09-20.
 */
var webpack = require('webpack');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var fs = require('fs');
var path = require('path');

var babelQuery = {
    presets: ['es2015', 'react', 'stage-0'],
    plugins: ['transform-runtime', 'add-module-exports', 'transform-decorators-legacy'],
    cacheDirectory: true,
};

module.exports = {
    context: path.resolve(__dirname, '..'),
    node: {
        __dirname: true
    },
    entry: {
        react: ['react', 'react-dom', 'react-router', 'react-redux', 'redux', 'react-router-redux', 'redux-thunk'],
        console: "./src/entries/index.js"
    },
    resolve: {
        root: [path.resolve('./src')],
        extensions: ['', '.js', '.jsx'],
    },
    output: {
        path: './dist/assets',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
        publicPath: '/assets/'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style", "css")
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style", "css!less")
            },
            {test: /\.jsx$/, exclude: /node_modules/, loader: 'babel', query: babelQuery,},
            {test: /\.js$/, exclude: /node_modules/, loader: 'babel', query: babelQuery,},
            {test: /\.svg$/, loader: "url-loader?limit=10000&mimetype=image/svg+xml"}
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production')
        }),
        new ExtractTextPlugin("[name].css", {
            allChunks: true
        }),
        new CommonsChunkPlugin("react", "react.js", ['console', 'site']),
        new CommonsChunkPlugin("commons", "commons.js", ['console', 'site', 'react']),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 15}),
        new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000})
    ]
};